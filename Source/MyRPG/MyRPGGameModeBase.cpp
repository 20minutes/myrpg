// Fill out your copyright notice in the Description page of Project Settings.
#include "MyRPGGameModeBase.h"
#include  "Public/RPGCharacter.h"
#include "RPGGameInstance.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/Engine.h"

AMyRPGGameModeBase::AMyRPGGameModeBase(const class FObjectInitializer& ObjectInitializer):
	Super(ObjectInitializer)
{
	DefaultPawnClass = ARPGCharacter::StaticClass();
	
	this->PrimaryActorTick.bCanEverTick = true;
	
}

void AMyRPGGameModeBase::BeginPlay()
{
	Cast<URPGGameInstance>(GetGameInstance())->Init();
}

void AMyRPGGameModeBase::Tick(float DeltaTime)
{
	if(this->currentCombatInstance != nullptr)
	{
		bool combatOver = this->currentCombatInstance->Tick(DeltaTime);

		if(combatOver)
		{
			if(this->currentCombatInstance->phase == CombatPhase::CPHASE_GameOver)
			{
				UE_LOG(LogTemp, Log, TEXT("Game Over"));
			}
			if (this->currentCombatInstance->phase == CombatPhase::CPHASE_Victory)
			{
				UE_LOG(LogTemp, Log, TEXT("Victory"));
			}

			UGameplayStatics::GetPlayerController(GetWorld(), 0)->bShowMouseCursor = false;
			

			//enable player actor
			UGameplayStatics::GetPlayerController( GetWorld(), 0) ->SetActorTickEnabled(true);


			this->CombatUIInstance->RemoveFromViewport();
			this->CombatUIInstance = nullptr;

			delete(this->currentCombatInstance);
			this->currentCombatInstance = nullptr;
			this->enemyParty.Empty();
		}
	}
}

void AMyRPGGameModeBase::TestCombat()
{
	//locate enemy assets
	UDataTable* enemyTable = Cast<UDataTable>(StaticLoadObject(UDataTable::StaticClass(), NULL, TEXT("DataTable'/Game/Data/Enemies.Enemies'")));

	if(enemyTable==nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("Enemies data table not found!"));
		return;
	}
	// locate enemy

	FEnemyInfo* row = enemyTable->FindRow<FEnemyInfo>( TEXT( "S1" ),      TEXT( "LookupEnemyInfo" ) );
	if (row == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("Enemy ID 'S1' not found!"));
		return;
	}
	// disable player actor
	UGameplayStatics::GetPlayerController( GetWorld(), 0 )-> SetActorTickEnabled( false );
	// add character to enemy party
	UGameCharacter* enemy = UGameCharacter::CreateGameCharacter(row, this );
	this->enemyParty.Add( enemy );

	URPGGameInstance* gameInstance = Cast<URPGGameInstance>(GetGameInstance());
	this->currentCombatInstance = new CombatEngine(gameInstance->PartyMembers, this->enemyParty);
	UE_LOG(LogTemp, Log, TEXT("Combat started"));

	this->CombatUIInstance = CreateWidget<UCombatUIWidget>(GetGameInstance(), this->CombatUIClass);

	if (this->CombatUIInstance) 
	{
		this->CombatUIInstance->AddToViewport();

		UGameplayStatics::GetPlayerController(GetWorld(), 0)->bShowMouseCursor = true;

		for (int i = 0; i < gameInstance->PartyMembers.Num(); i++)
		{
			this->CombatUIInstance->AddPlayerCharacterPanel(gameInstance->PartyMembers[i]);
		}

		for (int i = 0; i < this->enemyParty.Num(); i++)
		{
			this->CombatUIInstance->AddEnemyCharacterPanel(this->enemyParty[i]);
		}
	}


}
