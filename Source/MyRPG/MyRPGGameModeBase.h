// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameCharacter.h"
#include "GameFramework/GameModeBase.h"
#include "Combat/CombatEngine.h"
#include "UI/CombatUIWidget.h"
#include "MyRPGGameModeBase.generated.h"


/**
 * 
 */
UCLASS()
class MYRPG_API AMyRPGGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
public:
	
	AMyRPGGameModeBase(const class FObjectInitializer& ObjectInitializer);

	virtual void BeginPlay() override;

	virtual void Tick(float DeltaTime) override;

public:
	CombatEngine* currentCombatInstance;
	TArray<UGameCharacter*> enemyParty;

	UFUNCTION(Exec)
	void TestCombat();

	UPROPERTY()
	UCombatUIWidget* CombatUIInstance;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "UI")
	TSubclassOf<class UCombatUIWidget> CombatUIClass;
};
