// Fill out your copyright notice in the Description page of Project Settings.


#include "TestDecisionMaker.h"
#include "../Actions/TestCombatAction.h"

void TestDecisionMaker::BeginMakeDecision(UGameCharacter* character)
{
	UGameCharacter* target = character->SelectTarget();
	character->combatAction = new TestCombatAction(target);
}	

bool TestDecisionMaker::MakeDecision(float DeltaSeconds)
{
	return true;
}
