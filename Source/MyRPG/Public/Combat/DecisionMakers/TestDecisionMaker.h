// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "IDecisionMaker.h"

/**
 * 
 */
class MYRPG_API TestDecisionMaker : public IDecisionMaker
{
public:
    virtual void BeginMakeDecision(UGameCharacter* character) override;
	virtual bool MakeDecision(float DeltaSeconds) override;
};
