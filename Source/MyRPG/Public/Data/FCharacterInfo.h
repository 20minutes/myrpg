// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "FCharacterClassInfo.h"
#include "FCharacterInfo.generated.h"

/**
 * 
 */
USTRUCT(BlueprintType)
struct MYRPG_API FCharacterInfo : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

	
	UPROPERTY(BlueprintReadWrite, EditAnyWhere, Category = "CharacterInfo")
	FString Character_Name;

	UPROPERTY(BlueprintReadOnly, EditAnyWhere, Category = "CharacterInfo")
	FString Class_ID;
		
	
};
