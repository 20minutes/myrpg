// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Data/FCharacterInfo.h"
#include "Data/FCharacterClassInfo.h"
#include "Data/FEnemyInfo.h"
#include "Combat/Actions/ICombatAction.h"
#include "Combat/DecisionMakers/IDecisionMaker.h"
#include "GameCharacter.generated.h"

/**
 * 
 */
class CombatEngine;
class ICombatAction;
class IDecisionMaker;


UCLASS(BlueprintType)
class MYRPG_API UGameCharacter : public UObject
{
	GENERATED_BODY()

public:
	FCharacterClassInfo* ClassInfo;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "CharacterInfo")
	FString CharacterName;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "CharacterInfo")
	int32 MHP;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "CharacterInfo")
	int32 MMP;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "CharacterInfo")
	int32 HP;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "CharacterInfo")
	int32 MP;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "CharacterInfo")
	int32 ATK;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "CharacterInfo")
	int32 DEF;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "CharacterInfo")
	int32 LUCK;

	CombatEngine* combatInstance;
public:
	static UGameCharacter* CreateGameCharacter(FCharacterInfo* characterInfo, UObject* outer);
	static UGameCharacter* CreateGameCharacter(FEnemyInfo* enemyInfo, UObject* outer);

public:
	void BeginDestroy() override;

protected:
	float testDelayTimer;


public:
	void BeginMakeDecision();
	bool MakeDecision(float DeltaSeconds);
	void BeginExecuteAction();
	bool ExecuteAction(float DeltaSeconds);
public:
	ICombatAction* combatAction;
	IDecisionMaker* decisionMaker;

public:
	bool isPlayer;
	UGameCharacter* SelectTarget();
};
