// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "DataTableEditor/Private/SRowEditor.h"

#include "TestCustonData.generated.h"

USTRUCT(BlueprintType)
struct FTestCustonData : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()
public:
	UPROPERTY(BlueprintReadOnly, Category = "TestCustonData")
	int32 SomeNumber;
	
	UPROPERTY(BlueprintReadOnly, Category = "TestCustonData")
	FString SomeString;
	
};
