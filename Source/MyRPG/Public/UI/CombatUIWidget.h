// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "GameCharacter.h"

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "CombatUIWidget.generated.h"


/**
 * 
 */
UCLASS()
class MYRPG_API UCombatUIWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintImplementableEvent, Category = "Combat UI")
	void AddPlayerCharacterPanel(UGameCharacter* target);

	UFUNCTION(BlueprintImplementableEvent, Category = "Combat UI")
	void AddEnemyCharacterPanel(UGameCharacter* target);
	
};
