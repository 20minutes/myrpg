// Fill out your copyright notice in the Description page of Project Settings.


#include "Test.h"
#include "Engine/Engine.h"
#include "Public/TestCustonData.h"

// Sets default values
ATest::ATest()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ATest::BeginPlay()
{
	Super::BeginPlay();

	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Hello World"));

	if(dataTable != nullptr)
	{
		FTestCustonData* row = dataTable->FindRow<FTestCustonData>(TEXT("2"), TEXT("LookupTestCustonData"));
		FString someString = row->SomeString;
		UE_LOG(LogTemp, Warning, TEXT("%s"), *someString);
		
	}
	
}

// Called every frame
void ATest::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

